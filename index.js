const express = require('express')
const app = express()
const path = require('path')
const morgan = require('morgan')

//Settings
// app.set('port', 8081)

//CORS
// app.use((req, res, next) => {
//     res.header('Access-Control-Allow-Origin', '*')
//     res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
//     res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE')
//     res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE')
//     next()
// })


//Middleware
app.use(morgan('dev'))
app.use(express.urlencoded({extended: false}))


// //Routes
app.use('/api/candidates', require('./routes/index'))

// //Static
app.use(express.static(path.join(__dirname, 'public')))

app.get('/', function(req, res) {
    res.render('index')
})

app.route('/*').get(function(req, res) {
	return res.sendFile(path.join(__dirname, 'public/index.html'))
})


app.listen(8081, function (){
	console.log('Listening on port  8081')
})

module.exports = app