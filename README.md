# **Globalwork Frontend Test**

###  Desarrollé el frontend y backend para esta prueba. Aquí encontrarás el código respectivo de cada uno con comentarios muy breves sobre lo que hace cada caso.

Esta prueba fue desarrollada bajo las tecnologías:

*  **Vue.js**
*  **Vue-router**
*  **Vuetify 2**
*  **Node.js**
*  **RESTful Services / APIS**
*  **JSON**


Esta prueba contiene 2 ejercicios, en el toolbar encontrarás:

"**List of candidates**" Esta vista muestra los candidatos registrados, por medio de una card, en ella puede editar y eliminar la información del candidato, si hace clic en el nombre será redirigido a "/details", en esta vista puede modificar la información del candidato, así como realizar operaciones en sus referencias laborales.

 "**List of exercises**" Muestra 2 cards con el título, enunciado, y una imagen que hace referencia al ejercicio. También muestra un mensaje que indica "pasar cursor para ver el resultado", estas cards tienen un efecto de "flip" cuando se gira el cursor  muestra el resultado en la parte posterior.

La interfaz es muy minimalista, por lo que su manejo es fácil.

### Interfaces
Vista - home
![front](/uploads/c27a7c621381ae1ed2a89d8a9912a235/front.png)


Vista - List of candidates
![list-candidates](/uploads/52e04457f521f49e6dad5da01bf07688/list-candidates.png)

## Project setup
```
Enter the project directory
```

### Run npm install to download the dependencies specified in the package.json
```
npm install
```

### Run node app.js // Nodemon (npm run dev)**
```
node app.js
```

### Port
```
8081
```
