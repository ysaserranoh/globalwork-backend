const {Router} = require('express')
const router = Router()
const fs = require('fs')
var bodyParser = require('body-parser')
router.use(bodyParser.urlencoded({ extended: false }))
router.use(bodyParser.json())

//Get all candidates
router.get('/', async(req, res) => {
    try {
        const candidates = await JSON.parse(fs.readFileSync('candidates.json', 'utf-8'))
        res.status(200).json({ candidates })
    } catch (error) {
        res.status(400).json({ message: 'There was an error fetching that information - '+error })
    }
})

//Get one candidate
router.get('/:id', async(req, res) => {
    try {
        const candidates = await JSON.parse(fs.readFileSync('candidates.json', 'utf-8'))
        let candidate = candidates.find((candidate) => {
            return candidate.id === parseInt(req.params.id)
        })
        console.log(candidate)
        if (candidate) {
            res.status(200).json({ candidate: candidate, message: 'Success' })
        } else {
            res.status(404).json({ message: 'Not found' })
        }
    } catch (error) {
        res.status(400).json({ message: 'There was an error fetching that information - '+error })
    }
})

//Update information of a candidate
router.post('/update', async(req, res) => {
    const { id, name, last_name, phone, address, laboral_references } = req.body.candidate
    let candidates = await JSON.parse(fs.readFileSync('candidates.json', 'utf-8'))
    try {
        //If exists of the candidate, then update information
        if (id) {
            candidates.forEach((candidate) => {
                if (candidate.id === id) {
                    candidate.name = name
                    candidate.last_name = last_name
                    candidate.phone = phone
                    candidate.address = address
                    candidate.laboral_references = laboral_references
                }
            })
        } else {
            //If not exists of the candidate, then  new add
            res.status(400).json({ message: 'Not found' })
        }
        //We save in the JSON file
        const json_candidate = JSON.stringify(candidates)
        fs.writeFileSync('candidates.json', json_candidate, 'utf-8')
        res.status(200).json({ message: 'The candidate was saved successfully.' })
    
    } catch (error) {
        res.status(400).json({ message: 'There was an error fetching that information  - '+error})
    }
})

//Add, Delete, Update laboral references of the a candidate
router.post('/laboral_references', async(req, res) => {
    const candidateId = req.body.id
    const { id, company_name, contact_name, start_at, leave_at } = req.body.laboral_reference
    let candidates = await JSON.parse(fs.readFileSync('candidates.json', 'utf-8'))
    try {
        let candidateIndex = candidates.findIndex((candidate) => {
            return candidate.id === parseInt(candidateId)
        })
        console.log(candidateIndex)
        if (candidateIndex !== -1) {
            let laboralReferenceId = (candidates[candidateIndex].laboral_references || []).findIndex((laboral_reference) => {
                return laboral_reference.id === parseInt(id)
            })

            console.log(laboralReferenceId)

            if (laboralReferenceId !== -1) {
                candidates[candidateIndex].laboral_references[laboralReferenceId] = {
                    id,
                    company_name,
                    contact_name,
                    start_at,
                    leave_at  
                }
            } else {
                const lastIndex = (candidates[candidateIndex].laboral_references || []).length - 1
                const newId = lastIndex == - 1 ? 1 : candidates[candidateIndex].laboral_references[lastIndex].id + 1
                let newLaboralReference = {
                    id: newId,
                    company_name,
                    contact_name,
                    start_at,
                    leave_at 
                }
                candidates[candidateIndex].laboral_references = candidates[candidateIndex].laboral_references || []
                candidates[candidateIndex].laboral_references.push(newLaboralReference)
            }
            const json_candidate = JSON.stringify(candidates)
            fs.writeFileSync('candidates.json', json_candidate, 'utf-8')
            res.status(200).json({ message: 'The laboral reference was saved successfully.' })
        } else {
            res.status(404).json({ message: 'Not found' })
        }
    } catch (error) {
        res.status(400).json({ message: 'There was an error fetching that information  - '+error})
    }
})

//Delete laboral references of a candidate
router.post('/laboral_references/remove', async(req, res) => {
    const candidateId = req.body.id
    const referenceId = req.body.laboralReferenceId
    let candidates = await JSON.parse(fs.readFileSync('candidates.json', 'utf-8'))
    try {
        let candidateIndex = candidates.findIndex((candidate) => {
            return candidate.id === parseInt(candidateId)
        })
        if (candidateIndex !== -1) {
            let laboralReferenceIndex = (candidates[candidateIndex].laboral_references || []).findIndex((laboral_reference) => {
                return laboral_reference.id === parseInt(referenceId)
            })
            console.log(laboralReferenceIndex)

            if (laboralReferenceIndex !== -1) {
                const candidates = await JSON.parse(fs.readFileSync('candidates.json', 'utf-8'))
                let result = (candidates[candidateIndex].laboral_references || []).filter(laboral_reference => laboral_reference.id != referenceId)
                candidates[candidateIndex].laboral_references = result
                const json_candidates = JSON.stringify(candidates)
                fs.writeFileSync('candidates.json', json_candidates, 'utf-8')
                res.status(200).json({ message: 'The laboral reference was successfully deleted.'  })
            } else {
                res.status(404).json({ message: 'Laboral reference not found' })
            }
        } else {
            res.status(404).json({ message: 'Canditate not found' })
        }
    } catch (error) {
        res.status(400).json({ message: 'There was an error fetching that information  - '+error})
    }
})

//Delete a candidate 
router.post('/remove/:id', async(req, res) => {
    try {
        if (req.params.id) {  
            console.log(req.body.id)
            const candidates = await JSON.parse(fs.readFileSync('candidates.json', 'utf-8'))
            let result = candidates.filter(candidate => candidate.id != req.params.id)
            const json_candidates = JSON.stringify(result)
            fs.writeFileSync('candidates.json', json_candidates, 'utf-8')
            res.status(200).json({ message: 'the candidate was successfully deleted.'  })
        } else {
            res.status(404).json({ message: 'That candidate does not exist.' })
        }
    } catch (error) {
        res.status(400).json({ message: 'There was an error saving the candidate - '+error})
    }
})
module.exports = router